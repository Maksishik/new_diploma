package com.univer.workload.ui.views;

import com.univer.workload.model.Workload;
import com.univer.workload.model.dto.WorkloadDTO;
import com.univer.workload.service.ReporterService;
import com.univer.workload.service.WorkloadService;
import com.univer.workload.ui.MainLayout;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Tag("workload-view")
@Route(value = "", layout = MainLayout.class)
@PageTitle("Общая нагрузка")
@HtmlImport("frontend://views/workload-view.html")
public class WorkloadView extends PolymerTemplate<WorkloadView.Model> {
    private WorkloadService workloadService;
    private ReporterService reporterService;

    @Id("myCombobox")
    ComboBox comboBox;

        @Id("grid")
        Grid<WorkloadDTO> grid;

    @Id("createExcelFile")
    Button createBtn;



    @Autowired
    public WorkloadView(WorkloadService workloadService, ReporterService reporterService) {
        this.reporterService = reporterService;
        this.workloadService = workloadService;

        Model model  = getModel();
        List<String> val = new ArrayList<>();
        val.add("one");
        val.add("two");
        val.add("three");
        comboBox.setItems(val);
        comboBox.setValue(val.get(0));
        model.setTitleUniver("ГОУ ВПО ЛНР \"Луганский национальный университет имени Тараса Шевченко");
        model.setTitileSecondary("Распределение учебной работы  в часах на 2017/2018 учебный год");
        model.setTitleKaf("Кафедра информационных технологий и систем");

        createBtn.addClickListener(buttonClickEvent -> {
            reporterService.createExcelFile();
            Notification.show(
                    "Файл был создан",
                    2000, Notification.Position.TOP_END);
        });

        List<WorkloadDTO> list = workloadService.getAll();
        grid.setItems(list);

        grid.addColumn(WorkloadDTO::getFormOfStudy).setHeader("Форма обучения").setWidth("140px");
        grid.addColumn(WorkloadDTO::getFirstName).setHeader("Имя препода").setWidth("140px");
        grid.addColumn(WorkloadDTO::getLastName).setHeader("Фамилия препода").setWidth("140px");
        grid.addColumn(WorkloadDTO::getDepartName).setHeader("Кафедра").setWidth("140px");
        grid.addColumn(WorkloadDTO::getDiscName).setHeader("Предмет").setWidth("140px");
        grid.addColumn(WorkloadDTO::getSpecialityName).setHeader("Специальность").setWidth("140px");
        grid.addColumn(WorkloadDTO::getFaculName).setHeader("Факультет").setWidth("140px");
        grid.addColumn(WorkloadDTO::getCource).setHeader("Курс").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfStud).setHeader("Количетсво студентов").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfFlow).setHeader("Количетсво потоков").setWidth("140px");
        grid.addColumn(WorkloadDTO::getGroupName).setHeader("Группа").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfGrPract).setHeader("Кол. гр. практ").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfSubGr).setHeader("Кол. подгрупп").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfLecPlH).setHeader("Кол. лек. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfPractPlH).setHeader("Кол. практ. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfLabPlH).setHeader("Кол. лаб. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfConsultPlH).setHeader("Кол. консуль. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfSemPrrPlH).setHeader("Кол. курсовой. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfExamH).setHeader("Кол. экзамен. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfDiplomH).setHeader("Кол. диплом. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfGekH).setHeader("Кол. ГЕК. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getNumOfIndividH).setHeader("Кол. индивид. ч.").setWidth("140px");
        grid.addColumn(WorkloadDTO::getTotalH).setHeader("Общ. кол. ч").setWidth("140px");
    }

    public interface Model extends TemplateModel{
        void setTitleUniver(String d);
        void setTitileSecondary(String d);
        void setTitleKaf(String d);
    }
}
