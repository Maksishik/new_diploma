package com.univer.workload.ui.views;

import com.univer.workload.model.Department;
import com.univer.workload.model.dto.WorkloadDTO;
import com.univer.workload.service.DepartmentService;
import com.univer.workload.ui.MainLayout;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Route(value = "depart", layout = MainLayout.class)
@PageTitle("Справочник преподавателей")
@Tag("x-remote-data-example")
@HtmlImport("frontend://views/department-view.html")
public class DepartmentView extends PolymerTemplate<DepartmentView.Model> {
    private DepartmentService departmentService;

    //@Id("grid")
    //Grid<Department> grid;

    @Autowired
    public DepartmentView(DepartmentService departmentService) {
        /*this.departmentService = departmentService;
        List<Department> list = departmentService.getDepartmentList();
        grid.setItems(list);

        grid.addColumn(Department::getShortName).setHeader("Name");
        grid.addColumn(Department::getFullName).setHeader("Age");*/

    }

    public interface Model extends TemplateModel {

    }

}
