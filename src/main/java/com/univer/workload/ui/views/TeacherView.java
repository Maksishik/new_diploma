package com.univer.workload.ui.views;

import com.univer.workload.model.Workload;
import com.univer.workload.ui.MainLayout;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;

@Route(value = "teacher"/*, layout = MainLayout.class*/)
@PageTitle("Справочник преподавателей")
@Tag("teacher-view")
@HtmlImport("frontend://views/teacher-view.html")
public class TeacherView extends PolymerTemplate<TeacherView.Model> {

    public TeacherView() {

    }

    public interface Model extends TemplateModel{

    }
}
