package com.univer.workload.ui.views;

import com.univer.workload.ui.MainLayout;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Tag("login-view")
@Route(value = "login", layout = MainLayout.class)
@PageTitle("Общая нагрузка")
@HtmlImport("frontend://views/login-view.html")
public class LoginView extends PolymerTemplate<LoginView.LoginLodel> /*implements RouterLayout*/ {
    @Id("submit")
    Button button;

    @Id("userName")
    TextField userName;

    @Id("pass")
    TextField password;

    public LoginView() {
        /*button.setText("Enter");
        button.addClickListener(buttonClickEvent -> {
            Authentication auth = new UsernamePasswordAuthenticationToken(userName.getValue(),password.getValue());
            DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
            Authentication authenticated = daoAuthenticationProvider.authenticate(auth);
            SecurityContextHolder.getContext().setAuthentication(authenticated);
        });*/

    }

    public interface LoginLodel extends TemplateModel {

    }

}
