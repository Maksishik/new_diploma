package com.univer.workload.ui.views;

import com.univer.workload.model.dto.CoefValueDTO;
import com.univer.workload.model.dto.FormulaDTO;
import com.univer.workload.service.CalculatorService;
import com.univer.workload.ui.MainLayout;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Route(value = "calc", layout = MainLayout.class)
@PageTitle("Калькулятор")
@HtmlImport("frontend://views/calculator-view.html")
/*@Tag("calculator-view")*/
public class CalculatorView extends VerticalLayout {
    private CalculatorService calculatorService;

    @Autowired
    public CalculatorView(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
        Div                     header          = new Div();
        HorizontalLayout        comboLayout     = new HorizontalLayout();
        HorizontalLayout        mathBtnsLayout  = new HorizontalLayout();
        List<Button>            mathBtns        = new ArrayList<>();
        Button                  submit          = new Button("Рассчитать");
        TextArea                formula         = new TextArea("Формула");
        TextField               coefFormula     = new TextField();
        ComboBox<FormulaDTO>    tableCombmo     = getFormulaField();
        ComboBox<CoefValueDTO>  coefCombo       = getCoefItems();
        Button                  clearFormula    = new Button("Очистить");

        header.addClassName("calc-header");
        formula.addClassName("formula");

        mathBtns.add(new Button("="));
        mathBtns.add(new Button("+"));
        mathBtns.add(new Button("-"));
        mathBtns.add(new Button("*"));
        mathBtns.add(new Button("/"));

        for (Button btn : mathBtns) {
            btn.addClickListener(buttonClickEvent -> {
                formula.setValue(formula.getValue() + btn.getText());
                coefFormula.setValue(coefFormula.getValue() + btn.getText());
            });
        }

        tableCombmo.addValueChangeListener(event -> {
            if(!event.getSource().isEmpty()) {
                formula.setValue(formula.getValue()+event.getValue().getField());
            }
        });

        coefCombo.addValueChangeListener(event -> {
            if(!event.getSource().isEmpty()) {
                formula.setValue(formula.getValue()+event.getValue().getTitle() + "(" + event.getValue().getValue() + ")");
                coefFormula.setValue(coefFormula.getValue() + event.getValue().getValue());
            }
        });

        submit.addClickListener(buttonClickEvent -> {
            //String[] parts = formula.getValue().toString().split("=");

            calculatorService.calcFormula(tableCombmo.getValue().getTable(), tableCombmo.getValue().getField(), coefFormula.getValue());
        });

        clearFormula.addClickListener(buttonClickEvent -> {
           coefFormula.setValue("");
           formula.setValue("");
        });

        comboLayout.add(tableCombmo, coefCombo);

        for (Button btn : mathBtns)
            mathBtnsLayout.add(btn);

        header.add(new TextField("Введите год"), comboLayout, mathBtnsLayout);

        add(header, formula, submit, clearFormula);
    }

    private ComboBox<FormulaDTO> getFormulaField() {
        List<FormulaDTO> res = calculatorService.findTabAndField();
        ComboBox<FormulaDTO> coefValueDTOComboBox = new ComboBox<>("Результирующие поле");
        coefValueDTOComboBox.addClassName("coef-combo-list");

        coefValueDTOComboBox.setRenderer(TemplateRenderer.<FormulaDTO> of(
                "<div>[[item.field]]<br><small>таблица: [[item.table]]</small><hr></div>")
                .withProperty("field", FormulaDTO::getField)
                .withProperty("table", FormulaDTO::getTable));

        coefValueDTOComboBox.setItems(res);
        coefValueDTOComboBox.setItemLabelGenerator(FormulaDTO::getField);


        return coefValueDTOComboBox;
    }

    private ComboBox<CoefValueDTO> getCoefItems() {
        List<CoefValueDTO> res = calculatorService.getAll();
        ComboBox<CoefValueDTO> coefValueDTOComboBox = new ComboBox<>("Коэффициенты");
        coefValueDTOComboBox.addClassName("coef-combo-list");

        coefValueDTOComboBox.setRenderer(TemplateRenderer.<CoefValueDTO> of(
                "<div>[[item.title]]<br><small>Значение: [[item.value]]</small><br><small>Год: [[item.year]]</small></div>")
                .withProperty("title", CoefValueDTO::getTitle)
                .withProperty("value", CoefValueDTO::getValue)
                .withProperty("year", CoefValueDTO::getYear));

        coefValueDTOComboBox.setItems(res);
        coefValueDTOComboBox.setItemLabelGenerator(CoefValueDTO::getTitle);


        return coefValueDTOComboBox;
    }

    public interface Model extends TemplateModel {

    }
}
