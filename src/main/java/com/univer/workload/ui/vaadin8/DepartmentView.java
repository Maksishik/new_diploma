/*
package com.univer.workload.ui.vaadin8;

import com.univer.workload.model.Department;
import com.univer.workload.service.DepartmentService;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringView(name = DepartmentView.VIEW_NAME)
public class DepartmentView extends VerticalLayout implements View{
    public static final String VIEW_NAME = "department";

    @Autowired
    private DepartmentService departmentService;

    @PostConstruct
    void init() {
        setSizeFull();

        Binder<Department> studentBinder = new Binder<>();
        TextField fnameStudField = new TextField();
        TextField lnameStudField = new TextField();
        studentBinder.bind(fnameStudField, Department::getFullName, Department::setFullName);
        studentBinder.bind(lnameStudField, Department::getShortName, Department::setShortName);

        Grid<Department> studentGrid = new Grid<>();
        studentGrid.getEditor().setBinder(studentBinder).setEnabled(true);

        List<Department> studentList = departmentService.getDepartmentList();

        HorizontalLayout content = new HorizontalLayout();
        VerticalLayout studentLayout = new VerticalLayout();

        Button addStudentBtn = new Button("Add student", clickEvent -> {
            getUI().getNavigator().navigateTo(DepartmentView.VIEW_NAME);
        });

        content.setSizeFull();

        studentGrid.setItems(studentList);
        studentGrid.addColumn(Department::getFullName).setCaption("Name").setEditorComponent(fnameStudField, Department::setFullName);
        studentGrid.addColumn(Department::getShortName).setCaption("Last name").setEditorComponent(lnameStudField, Department::setShortName);
        studentGrid.setSizeFull();

        studentGrid.getEditor().addSaveListener(editorSaveEvent -> {
            try {
                studentBinder.writeBean(editorSaveEvent.getBean());
                departmentService.addDepartment(editorSaveEvent.getBean());
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });

        studentLayout.addComponents(studentGrid);
        studentLayout.setSizeFull();

        content.addComponents(studentLayout);
        addComponents(content, addStudentBtn);
    }
}
*/
