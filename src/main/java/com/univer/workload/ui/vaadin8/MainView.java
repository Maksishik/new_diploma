/*
package com.univer.workload.ui.vaadin8;

import com.vaadin.annotations.Theme;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.*;


@SpringUI
@Theme("valo")
@SpringViewDisplay
public class MainView extends UI implements ViewDisplay{
    public static final String VIEW_NAME = "main";

    Panel conentPanel = new Panel();

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Panel sidePanel = new Panel();
        HorizontalLayout root = new HorizontalLayout();
        VerticalLayout sidePanelContent = new VerticalLayout();
        Label label = new Label("Petya Petrov");
        label.setIcon(FontAwesome.USER);

        sidePanelContent.addComponent(label);
        sidePanelContent.addComponent(getTree());

        root.setSizeFull();

        setContent(root);

        root.addComponents(sidePanel, conentPanel);

        sidePanel.setHeight("100%");
        sidePanel.setCaption("Меню");
        sidePanel.setContent(sidePanelContent);

        conentPanel.setHeight("100%");

        root.setExpandRatio(sidePanel, 0.2f);
        root.setExpandRatio(conentPanel, 0.7f);

        getNavigator().navigateTo(DepartmentView.VIEW_NAME);
    }

    @Override
    public void showView(View view) {
        conentPanel.setContent((Component) view);
    }

    private Tree getTree() {
        Tree<String> tree = new Tree<>();
        TreeData<String> treeData = new TreeData<>();

// Couple of childless root items
        treeData.addItem(null,"Mercury");
        treeData.addItem("Mercury","LALLA");
        treeData.addItem(null,"Venus");
        treeData.addItem("Venus","SASU");

// Items with hierarchy
        treeData.addItem(null,"Earth");
        treeData.addItem("Earth","The Moon");

        TreeDataProvider<String> inMemoryDataProvider = new TreeDataProvider<>(treeData);
        tree.setDataProvider(inMemoryDataProvider);
        tree.expand("Earth"); // Expand programmatically

        return tree;
    }
}
*/
