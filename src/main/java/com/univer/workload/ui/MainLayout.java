package com.univer.workload.ui;

import com.univer.workload.ui.views.CalculatorView;
import com.univer.workload.ui.views.DepartmentView;
import com.univer.workload.ui.views.TeacherView;
import com.univer.workload.ui.views.WorkloadView;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcons;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.InitialPageSettings;
import com.vaadin.flow.server.PageConfigurator;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Tag("main-view")
@HtmlImport("frontend://views/mainView.html")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@BodySize(height = "100vh", width = "100vw")
@Theme(Lumo.class)
public class MainLayout extends PolymerTemplate<MainLayout.MainModel> implements RouterLayout {

    public MainLayout() {
        MainModel model = getModel();
        model.setUserName("Василий Петоров");
    }

    public interface MainModel extends TemplateModel{
        void setUserName(String d);
    }
}
