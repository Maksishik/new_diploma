/*
package com.univer.workload.repository.jooq;

import calculate.tables.pojos.Workload;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WorkloadRepositoryJooq {
    private final DSLContext create;

    @Autowired
    public WorkloadRepositoryJooq(DSLContext create) {
        this.create = create;
    }

    public List<?> getAll() {
        List<Workload> res = create.select(calculate.tables.Workload.WORKLOAD.FORM_OF_STUDY,
                calculate.tables.Workload.WORKLOAD.TEACHER_ID,
                calculate.tables.Workload.WORKLOAD.COURCE,
                calculate.tables.Workload.WORKLOAD.NUM_OF_STUD).from(calculate.tables.Workload.WORKLOAD).fetch().into(Workload.class);

        return res;
    }
}
*/
