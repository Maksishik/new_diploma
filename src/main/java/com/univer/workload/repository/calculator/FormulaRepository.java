package com.univer.workload.repository.calculator;

import com.univer.workload.model.Formula;
import com.univer.workload.model.dto.FormulaDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface FormulaRepository extends JpaRepository<Formula, Long> {
    /*@Query("select formul from formula where formula.table = :table and formula.field = :field")
    Formula getFormulaByTableAndField(@Param("table") String table, @Param("field") String field);*/

    Formula findByTabAndField(String table, String field);

    @Query("select new com.univer.workload.model.dto.FormulaDTO(f.tab, f.field) " +
            "FROM Formula f")
    List<FormulaDTO> findTabAndField();

    @Modifying
    @Query(value = "insert into formula(tab, field, formul) select :tab, :field, :formul", nativeQuery = true)
    @org.springframework.transaction.annotation.Transactional
    void saveFormula(@Param("tab") String table, @Param("field") String field, @Param("formul") String formula);
}
