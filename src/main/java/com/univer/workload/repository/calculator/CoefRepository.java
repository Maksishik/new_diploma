package com.univer.workload.repository.calculator;

import com.univer.workload.model.Coef;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoefRepository extends JpaRepository<Coef, Long> {
}
