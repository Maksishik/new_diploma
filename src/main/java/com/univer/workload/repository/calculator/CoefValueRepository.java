package com.univer.workload.repository.calculator;

import com.univer.workload.model.CoefValue;
import com.univer.workload.model.dto.CoefValueDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoefValueRepository extends JpaRepository<CoefValue, Long> {
    @Query("select new com.univer.workload.model.dto.CoefValueDTO(cv.value, c.code, cv.year, c.title) " +
            "FROM CoefValue cv " +
            "join cv.coefName c")
    List<CoefValueDTO> getAllCoef();
}
