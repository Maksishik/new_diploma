package com.univer.workload.repository;

import com.univer.workload.model.Department;
import com.univer.workload.model.dto.WorkloadDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    //DepartmentView getDepartmentByFullNameAfter();
    /*@Query("select new com.univer.workload.model.dto.WorkloadDTO(w.formOfStudy, t.firstName, t.lastName, w.cource, w.numOfStud, w.numOfFlow, g.name) " +
            "FROM Workload w " +
            "join w.teacherName t " +
            "join w.groupName g")
    List<WorkloadDTO> getAll();*/
}
