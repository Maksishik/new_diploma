package com.univer.workload.repository;

import com.univer.workload.model.Workload;
import com.univer.workload.model.Teacher;
import com.univer.workload.model.dto.WorkloadDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkloadRepository extends JpaRepository<Workload, Long> {
    @Query("select new com.univer.workload.model.dto.WorkloadDTO(w.formOfStudy, t.firstName, " +
            "t.lastName, w.cource, w.numOfStud, w.numOfFlow, g.name) " +
            "FROM Workload w " +
            "join w.teacherName t " +
            "join w.groupName g")
    List<WorkloadDTO> getSomeInfo();

    @Query("select new com.univer.workload.model.dto.WorkloadDTO(" +
            "w.id, w.formOfStudy, t.firstName, t.lastName, dep.shortName, dis.shortName, " +
            "spec.name, fac.shortName, w.cource, w.numOfStud, w.numOfFlow, g.name, w.numOfGrPract," +
            "w.numOfSubGr, w.numOfLecPlHour, w.numOfPractPlHour, " +
            "w.numOfLabPlHour, w.numOfConsultPlHour, w.numOfSemPrrPlHour, w.numOfExamHour, " +
            "w.numOfDiplomHour, w.numOfGekHour, w.numOfIndividHour, w.totalHour) " +
            "FROM Workload w " +
            "join w.teacherName t " +
            "join w.groupName g " +
            "join w.departName dep " +
            "join w.discName dis " +
            "join w.specialityName spec " +
            "join w.faculName fac")
    List<WorkloadDTO> getAll();

    @Query("select new com.univer.workload.model.dto.WorkloadDTO(t.firstName, t.lastName) " +
            "from Workload w " +
            "join w.teacherName t where w.id = :paramId")
    WorkloadDTO byId(@Param("paramId") Long paramId);
}
