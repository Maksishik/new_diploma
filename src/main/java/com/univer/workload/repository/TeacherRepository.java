package com.univer.workload.repository;

import com.univer.workload.model.Teacher;
import com.univer.workload.model.dto.TeacherDTO;
import com.univer.workload.model.dto.WorkloadDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    @Query("select new com.univer.workload.model.dto.TeacherDTO(" +
            "t.id, t.firstName, t.lastName, p.name, d.shortName, s.name) " +
            "FROM Teacher t " +
            "join t.postName p " +
            "join t.departName d " +
            "join t.secondJob s")
    List<TeacherDTO> getAll();
}
