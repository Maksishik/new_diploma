package com.univer.workload.repository;

import com.univer.workload.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    List<Group> getGroupsBySpecialityName(String specialityName);
}
