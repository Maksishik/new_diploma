package com.univer.workload.service;

import com.univer.workload.model.Formula;
import com.univer.workload.repository.calculator.FormulaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

@Service
public class FormulaService {
    private FormulaRepository formulaRepository;

    @Autowired
    public FormulaService(FormulaRepository formulaRepository) {
        this.formulaRepository = formulaRepository;
    }

    public Formula getFormulaByTableField(String table, String field) {
        test(table, field);
        return formulaRepository.findByTabAndField(table, field);
    }

    private void test(String table, String field) {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        Formula formula = new Formula();
        formula = formulaRepository.findByTabAndField(table, field);
        try {
            Object res = engine.eval(formula.getFormul());
            formula.setResult(res.getClass() == Integer.class ? (Integer) res : (Float) res);
            formulaRepository.save(formula);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}
