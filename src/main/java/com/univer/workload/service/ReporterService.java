package com.univer.workload.service;

import com.univer.workload.io.ExcelReporter;
import org.springframework.stereotype.Service;

@Service
public class ReporterService {
    public void createExcelFile() {
        ExcelReporter.writeExcel();
    }
}
