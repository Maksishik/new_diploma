package com.univer.workload.service;

import com.univer.workload.model.Workload;
import com.univer.workload.model.dto.WorkloadDTO;
import com.univer.workload.repository.WorkloadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope(value = "prototype")
public class WorkloadService {
    private WorkloadRepository workloadRepository;
    //private WorkloadRepositoryJooq workloadRepositoryJooq;

    public WorkloadService() {}

    @Autowired
    public WorkloadService(WorkloadRepository workloadRepository/*, WorkloadRepositoryJooq workloadRepositoryJooq*/) {
        this.workloadRepository = workloadRepository;
        //this.workloadRepositoryJooq = workloadRepositoryJooq;
    }

    public List<WorkloadDTO> getAll() { return workloadRepository.getAll(); }

    public List<WorkloadDTO> getWorkloadlist() {
        return workloadRepository.getAll();
    }

    public List<Workload> findAll() {return workloadRepository.findAll();}

    public List<WorkloadDTO> getSomeInfo() {return workloadRepository.getSomeInfo();}

    public WorkloadDTO getById(Long id) {
        return workloadRepository.byId(id);
    }

    /*public List<?> findAll() {
        return workloadRepositoryJooq.getAll();
    }*/
}
