package com.univer.workload.service;

import com.univer.workload.model.Formula;
import com.univer.workload.model.dto.CoefValueDTO;
import com.univer.workload.model.dto.FormulaDTO;
import com.univer.workload.repository.calculator.CoefRepository;
import com.univer.workload.repository.calculator.CoefValueRepository;
import com.univer.workload.repository.calculator.FormulaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;

@Service
public class CalculatorService {
    private CoefRepository coefRepository;
    private CoefValueRepository coefValueRepository;
    private FormulaRepository formulaRepository;

    @Autowired
    public CalculatorService(CoefRepository coefRepository, CoefValueRepository coefValueRepository, FormulaRepository formulaRepository) {
        this.coefRepository = coefRepository;
        this.coefValueRepository = coefValueRepository;
        this.formulaRepository = formulaRepository;
    }

    public List<CoefValueDTO> getAll() {
        return coefValueRepository.getAllCoef();
    }

    public List<FormulaDTO> findTabAndField() {
        return formulaRepository.findTabAndField();
    }

    public void calcFormula(String table, String field, String formula) {
        Formula formul = new Formula();
        formul.setTab(table);
        formul.setField(field);
        formul.setFormul(formula);
        formul.setResult(getFormulaRes(formul.getFormul()).getClass() == Integer.class ?
                                                                        (Integer) getFormulaRes(formul.getFormul()) :
                                                                        (Float)getFormulaRes(formul.getFormul()));
        formulaRepository.save(formul);
    }

     private Object getFormulaRes(String formula) {
         ScriptEngineManager mgr = new ScriptEngineManager();
         ScriptEngine engine = mgr.getEngineByName("JavaScript");
         try {
             Object res = engine.eval(formula);
             return res;
         } catch (ScriptException e) {
             e.printStackTrace();
         }
         return null;
     }
}
