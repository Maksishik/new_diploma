package com.univer.workload.service;

import com.univer.workload.model.Group;
import com.univer.workload.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupService {
    private GroupRepository groupRepository;

    @Autowired
    public GroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public List<Group> getDepartmentList() {
        return groupRepository.findAll();
    }

    public List<Group> getGroupsBySpeciality(String name) {
        return groupRepository.getGroupsBySpecialityName(name);
    }
}
