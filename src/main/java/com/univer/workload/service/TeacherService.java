package com.univer.workload.service;

import com.univer.workload.model.dto.TeacherDTO;
import com.univer.workload.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    private TeacherRepository teacherRepository;

    @Autowired
    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<TeacherDTO> getAll() {
        return teacherRepository.getAll();
    }
}
