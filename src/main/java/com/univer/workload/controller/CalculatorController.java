package com.univer.workload.controller;

import com.univer.workload.model.Formula;
import com.univer.workload.service.CalculatorService;
import com.univer.workload.service.FormulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/calc")
public class CalculatorController {
    private FormulaService      formulaService;
    private CalculatorService   calculatorService;

    @Autowired
    public CalculatorController(FormulaService formulaService, CalculatorService calculatorService) {
        this.formulaService = formulaService;
        this.calculatorService = calculatorService;
    }

    @GetMapping(value = "/getFormulaByField/{table}/{field}")
    public Formula getFormulaByField(@PathVariable(value = "table") String table,
                                     @PathVariable(value = "field") String field) {
        return formulaService.getFormulaByTableField(table, field);
    }

    @PostMapping(value = "/calculate/{table}/{field}/{formula}")
    public void calcFormula(@PathVariable(value = "table") String table,
                             @PathVariable(value = "field") String field,
                             @PathVariable(value = "formula") String formula) {
        calculatorService.calcFormula(table, field, formula);
    }
}
