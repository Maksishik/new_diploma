package com.univer.workload.controller;

import com.univer.workload.model.Group;
import com.univer.workload.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/groups")
public class GroupController {
    private GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping(value = "/getAll")
    public List<Group> getAll() {
        return groupService.getDepartmentList();
    }

    @GetMapping(value = "/getBySpeciality/{name}")
    public List<Group> getBySpeciality(@PathVariable(value = "name") String name) {
        return groupService.getGroupsBySpeciality(name);
    }
}
