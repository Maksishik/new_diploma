package com.univer.workload.controller;

import com.univer.workload.model.dto.WorkloadDTO;
import com.univer.workload.service.WorkloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/workload")
public class WorkloadController {
    private WorkloadService workloadService;

    @Autowired
    public WorkloadController(WorkloadService workloadService) {
        this.workloadService = workloadService;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping(value = "/getAll")
    public List<WorkloadDTO> getAll() {
        return workloadService.getAll();
    }
}
