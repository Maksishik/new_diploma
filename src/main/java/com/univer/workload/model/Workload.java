package com.univer.workload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "workload")
@AllArgsConstructor
@NoArgsConstructor
public class Workload {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String formOfStudy;

    @JsonIgnore
    @JoinColumn(name = "teacher_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Teacher teacherName;

    @JsonIgnore
    @JoinColumn(name = "dep_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Department  departName;

    @JsonIgnore
    @JoinColumn(name = "disc_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Discipline discName;

    @JsonIgnore
    @JoinColumn(name = "spec_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Speciality  specialityName;

    @JsonIgnore
    @JoinColumn(name = "facul_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Faculty faculName;

    private Integer cource;
    private Integer numOfStud;
    private Integer numOfFlow;

    @JsonIgnore
    @JoinColumn(name = "group_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Group groupName;

    private Integer  numOfGrPract;
    private Integer  numOfSubGr;

    //@JoinColumn(name = "num_of_lec_pl_hour")
    private Integer  numOfLecPlHour;

    //@JoinColumn(name = "num_of_pract_pl_h")
    private Integer  numOfPractPlHour;

    //@JoinColumn(name = "num_of_lab_pl_h")
    private Integer  numOfLabPlHour;

    //@JoinColumn(name = "num_of_consult_pl_h")
    private Integer  numOfConsultPlHour;

    //@JoinColumn(name = "num_of_sem_prr_pl_h")
    private Integer  numOfSemPrrPlHour;

    private Integer  numOfExamHour;
    private Integer  numOfDiplomHour;
    private Integer  numOfGekHour;
    private Integer  numOfIndividHour;
    private Double  totalHour;

}
