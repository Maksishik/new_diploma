package com.univer.workload.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkloadDTO {
    /*23*/
    private Long    id;
    private String  formOfStudy;
    private String  firstName;
    private String  lastName;
    private String  departName;
    private String  discName;
    private String  specialityName;
    private String  faculName;
    private Integer cource;
    private Integer numOfStud;
    private Integer numOfFlow;
    private String  groupName;
    private Integer  numOfGrPract;
    private Integer  numOfSubGr;
    private Integer  numOfLecPlH;
    private Integer  numOfPractPlH;
    private Integer  numOfLabPlH;
    private Integer  numOfConsultPlH;
    private Integer  numOfSemPrrPlH;
    private Integer  numOfExamH;
    private Integer  numOfDiplomH;
    private Integer  numOfGekH;
    private Integer  numOfIndividH;
    private Double  totalH;
    //private Double  numOfSemH;

    public WorkloadDTO(String formOfStudy, String firstName, String lastName, Integer cource, Integer numOfStud, Integer numOfFlow, String groupName) {
        this.formOfStudy = formOfStudy;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cource = cource;
        this.numOfStud = numOfStud;
        this.numOfFlow = numOfFlow;
        this.groupName = groupName;
    }

    public WorkloadDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
