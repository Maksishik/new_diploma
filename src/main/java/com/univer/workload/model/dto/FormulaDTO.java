package com.univer.workload.model.dto;

public class FormulaDTO {
    private String table;
    private String field;
    private String formula;
    private Double result;

    public FormulaDTO() {}

    public FormulaDTO(String table, String field) {
        this.table = table;
        this.field = field;
    }

    public FormulaDTO(String table, String field, String formula) {
        this.table = table;
        this.field = field;
        this.formula = formula;
    }

    public FormulaDTO(String table, String field, String formula, Double result) {
        this.table = table;
        this.field = field;
        this.formula = formula;
        this.result = result;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }
}
