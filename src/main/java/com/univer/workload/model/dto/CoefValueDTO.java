package com.univer.workload.model.dto;

public class CoefValueDTO {
    private Double value;
    private String code;
    private String year;
    private String title;

    public CoefValueDTO(Double value, String code, String year, String title) {
        this.value = value;
        this.code = code;
        this.year = year;
        this.title = title;
    }

    public CoefValueDTO(Double value, String code, String title) {
        this.value = value;
        this.title = title;
        this.code = code;
    }

    public CoefValueDTO(String code, Double value, String year) {
        this.value = value;
        this.code = code;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getName() {
        return code;
    }

    public void setName(String name) {
        this.code = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
