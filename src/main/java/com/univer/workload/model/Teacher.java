package com.univer.workload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "teacher")
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @JsonIgnore
    @JoinColumn(name = "post_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Post  postName;

    @JsonIgnore
    @JoinColumn(name = "dep_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Department  departName;

    @JsonIgnore
    @JoinColumn(name = "sec_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SecondJob  secondJob;

}
