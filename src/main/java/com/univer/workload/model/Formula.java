package com.univer.workload.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "formula")
@AllArgsConstructor
@NoArgsConstructor
public class Formula {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String tab;
    private String field;
    private String formul;
    private Float result;


}
