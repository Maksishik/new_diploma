package com.univer.workload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "coef_value")
@AllArgsConstructor
@NoArgsConstructor
public class CoefValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double value;

    @JsonIgnore
    @JoinColumn(name = "coef_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Coef coefName;

    private String year;
}
