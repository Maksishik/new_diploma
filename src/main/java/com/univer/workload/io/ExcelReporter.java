package com.univer.workload.io;

import com.univer.workload.model.dto.WorkloadDTO;
import com.univer.workload.service.WorkloadService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@ComponentScan("com.univer.workload.service")
public class ExcelReporter {
    private static WorkloadService workloadService;
    private static String inputFile    = "C:/Users/Maksim/Dropbox/Диплом/Проект/пример/new/workload/src/main/resources/excel/template.xls";
    private static String outputFile = "C:/Users/Maksim/Dropbox/Диплом/Проект/пример/new/workload/src/main/resources/excel/workload.xls";
    private static List<WorkloadDTO> workloadList = null;

    public ExcelReporter(String inputFile, String outputFile) {
        this.inputFile  = inputFile;
        this.outputFile = outputFile;
    }

    @Autowired
    public ExcelReporter(WorkloadService workloadService) {
        this.workloadService = workloadService;
        workloadList = workloadService.getAll();
    }

    private static Sheet uploadTemplate() {
        Sheet sheet = null;
        try (FileInputStream fs = new FileInputStream(new File(inputFile))){
            Workbook wb = new HSSFWorkbook(fs);
            sheet = wb.getSheetAt(1);
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sheet;
    }

    private static void createOutFile(Workbook wb) {
        FileOutputStream outFile;
        try {
            outFile = new FileOutputStream(new File(outputFile));
            wb.write(outFile);
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeExcel() {
        Sheet sheet = uploadTemplate();

        for (Row row : sheet) {
            //Iterator<Cell> cellIterator = row.cellIterator();
            for (Cell cell : row) {
                //Cell cell = cellIterator.next();
                if (cell.getCellTypeEnum() == CellType.STRING) {
                    String[] data = checkAndSplitBytRegEx(cell.getStringCellValue());

                    if (data != null) {
                        String methodName = data[0];

                        List<String> dataSource = getWorkloadListByName(methodName);

                        fillExcel(sheet, row.getRowNum(), cell.getColumnIndex(), dataSource);

                        createOutFile(sheet.getWorkbook());
                    }
                }
            }
        }
    }

    private static List<String> getWorkloadListByName(String methodName) {
        List<String> res = new ArrayList<>();
        for (WorkloadDTO w : workloadList) {
            try {
                if (w.getClass().getDeclaredMethod(methodName).invoke(w) != null) {
                    String methodValue = w.getClass().getDeclaredMethod(methodName).invoke(w).toString();
                    res.add(methodValue);
                } else
                    res.add("");
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }

        return true;
    }

    private static void fillExcel(Sheet sheet, int rowNum, int cellNum, List<String> dataSource) {
        Row row;
        Cell cell;
        int i = 0;

        for (String obj : dataSource) {
            row = sheet.getRow(rowNum + i);
            cell = row.getCell(cellNum);

            if (isInteger(obj))
                cell.setCellValue(Integer.parseInt(obj));
            else
                cell.setCellValue(obj);

            i++;
        }
    }

    private static String[] checkAndSplitBytRegEx(String text) {
        String pattern = "\\$\\{([aA-zZ]+)}";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);


        if(m.matches())
            return new String[]{m.group(1)/*, m.group(3)*/};
        else
            return null;
    }
}