INSERT INTO roles(name) VALUES ('admin');
INSERT INTO roles(name) VALUES ('teacher');

INSERT INTO users(name, pass, role_id) VALUES ('denis', '123', 2);
INSERT INTO users(name, pass, role_id) VALUES ('vasya', '123', 2);
INSERT INTO users(name, pass, role_id) VALUES ('petya', '123', 2);
INSERT INTO users(name, pass, role_id) VALUES ('maksim', '123', 1);

INSERT INTO faculty(short_name, full_name) VALUES ('ИФМИТ', 'Институт физики, математики и информационных технологий');

INSERT INTO department(short_name, full_name) VALUES ('ИТС', 'Информационных технологий и систем');

INSERT INTO speciality (name, dep_id) VALUES ('ПИ', 1);
INSERT INTO speciality (name, dep_id) VALUES ('КИ', 1);

INSERT INTO groups(name, facul_id, spec_id, num_of_stud) VALUES ('4-ПИ', 2, 1, 13);
INSERT INTO groups(name, facul_id, spec_id, num_of_stud) VALUES ('2-ПИ', 2, 1, 24);
INSERT INTO groups(name, facul_id, spec_id, num_of_stud) VALUES ('3-КИ', 2, 2, 24);

INSERT INTO post (name, vacancy_rate) VALUES ('Доцент', 1);
INSERT INTO post (name, vacancy_rate) VALUES ('Старший преподаватель', 1);

INSERT INTO teacher (first_name, last_name, post_id, dep_id) VALUES ('Юрий', 'Шкандыбин', 2, 1);

/*Calculation values*/
INSERT INTO coef (title, code) VALUES ('Количество потоков', 'num_of_flow');
INSERT INTO coef_value (value, coef_id, year) VALUES ('1', 1, '2016');
INSERT INTO formula (tab, field, formula, result) VALUES ('workload', 'num_of_lec_h', '2*Количество потоков', 12.2);



