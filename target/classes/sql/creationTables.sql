CREATE TABLE roles(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE users(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  pass VARCHAR(255),
  role_id INT,
  FOREIGN KEY (role_id) REFERENCES roles(id)
);

CREATE TABLE department(
  id BIGSERIAL PRIMARY KEY,
  short_name VARCHAR(255),
  full_name VARCHAR(255)
);

CREATE TABLE speciality(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  dep_id INT,
  FOREIGN KEY (dep_id) REFERENCES department(id)
);

CREATE TABLE faculty(
  id BIGSERIAL PRIMARY KEY,
  short_name VARCHAR(255),
  full_name VARCHAR(255)
);

CREATE TABLE groups(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  facul_id INT,
  spec_id INT,
  num_of_stud INT,
  FOREIGN KEY (facul_id) REFERENCES faculty(id),
  FOREIGN KEY (spec_id) REFERENCES speciality(id)
);

/*CREATE TABLE post(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  vacancy_rate FLOAT
);

CREATE TABLE second_job(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  vacancy_rate FLOAT
);*/

CREATE TABLE teacher(
  id BIGSERIAL PRIMARY KEY,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  post_id INT,
  dep_id INT,
  sec_job_id INT,
  FOREIGN KEY (post_id) REFERENCES post(id),
  FOREIGN KEY (dep_id) REFERENCES department(id),
  FOREIGN KEY (sec_job_id) REFERENCES second_job(id)
);

CREATE TABLE discipline(
  id BIGSERIAL PRIMARY KEY,
  short_name VARCHAR(255),
  full_name VARCHAR(255),
  dep_id INT,
  FOREIGN KEY (dep_id) REFERENCES department(id)
);

CREATE TABLE workload(
  id BIGSERIAL PRIMARY KEY,
  form_of_study VARCHAR(255),
  teacher_id INT,
  dep_id INT,
  disc_id INT,
  spec_id INT,
  facul_id INT,
  cource INT,
  num_of_stud INT,
  num_of_flow INT,
  group_id INT,
  num_of_gr_pract INT,
  num_of_sub_gr INT,
  num_of_lec_pl_hour FLOAT,
  num_of_pract_pl_hour FLOAT,
  num_of_lab_pl_hour FLOAT,
  num_of_consult_pl_hour FLOAT,
  num_of_sem_prr_pl_hour FLOAT,
  num_of_exam_hour FLOAT,
  num_of_diplom_hour FLOAT,
  num_of_gek_hour FLOAT,
  num_of_individ_hour FLOAT,
  total_hour FLOAT,
  FOREIGN KEY (teacher_id) REFERENCES teacher(id),
  FOREIGN KEY (dep_id) REFERENCES department(id),
  FOREIGN KEY (disc_id) REFERENCES discipline(id),
  FOREIGN KEY (spec_id) REFERENCES speciality(id),
  FOREIGN KEY (facul_id) REFERENCES faculty(id)
);

CREATE TABLE dep_staffing(
  id BIGSERIAL PRIMARY KEY,
  post_id INT,
  quantity FLOAT,
  FOREIGN KEY (post_id) REFERENCES post(id)
);

CREATE TABLE workload_teacher(
  id BIGSERIAL PRIMARY KEY,
  teacher_id INT,
  post_id INT,
  second_job_id INT,
  total_h INT,
  FOREIGN KEY (post_id) REFERENCES post(id),
  FOREIGN KEY (second_job_id) REFERENCES second_job(id),
  FOREIGN KEY (teacher_id) REFERENCES teacher(id)
);


/*Calculation tables*/
CREATE TABLE coef(
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(255),
  code VARCHAR(255)
);

CREATE TABLE coef_value(
  id BIGSERIAL PRIMARY KEY,
  value FLOAT,
  coef_id INT,
  year VARCHAR(255),
  FOREIGN KEY (coef_id) REFERENCES coef(id)
);

CREATE TABLE formula(
  id BIGSERIAL PRIMARY KEY,
  tab VARCHAR(255),
  field VARCHAR(255),
  formula VARCHAR(255),
  result FLOAT
);

CREATE TABLE formula_coef(
  id BIGSERIAL PRIMARY KEY,
  coef_id INT,
  formula_id INT,
  FOREIGN KEY (coef_id) REFERENCES coef_value(id),
  FOREIGN KEY (formula_id) REFERENCES formula(id)
);

